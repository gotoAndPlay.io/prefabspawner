using System;
using System.Collections.Generic;
using GotoAndPlay.Unity3D.Pool;
using UnityEngine;

namespace GotoAndPlay.Unity3D.PrefabSpawner
{
    public interface IPrefabSpawner
    {
        void AddPool(string ID, PrefabPool pool);
        void Init(bool initAllPools = true);
        void InitPool(string poolID);
        GameObject Spawn(string poolID, Transform parent, Vector3 position, Quaternion rotation);
        void Despawn(GameObject item);
        void Reset();
        List<PrefabPool> Pools { get; }
    }
    
    [CreateAssetMenu(fileName = "PrefabSpawner", menuName = "GotoAndPlay/Pool/PrefabSpawner", order = 2)]
    public class PrefabSpawner: ScriptableObject, IPrefabSpawner
    {
        #region Properties

        [SerializeField] private List<PrefabPool> _pools = new List<PrefabPool>();
        
        private Dictionary<string, PrefabPool> _poolsByID = new Dictionary<string, PrefabPool>();
        private Dictionary<string, string> _gameObjectNameToPoolID = new Dictionary<string, string>();
        
        #endregion

        #region Getter & Setter

        #endregion

        #region IPrefabSpawner implementation
        
        public List<PrefabPool> Pools
        {
            get => _pools;
        }
        
        public GameObject Spawn(string poolID, Transform parent, Vector3 position, Quaternion rotation) {
            if(!_poolsByID.ContainsKey(poolID))
                throw new Exception($"PrefabSpawner.Spawn: unknown '{poolID}'");
            
            var go = _poolsByID[poolID].Spawn(parent, position, rotation);
            
            if(_gameObjectNameToPoolID.ContainsKey(go.name))
                throw new Exception($"PrefabSpawner.Spawn: gameobject name collision '{go.name}'");
            
            _gameObjectNameToPoolID.Add(go.name, poolID);

            return go;
        }

        public void Despawn(GameObject item) {
            if(!_gameObjectNameToPoolID.ContainsKey(item.name))
                throw new Exception($"PrefabSpawner.Despawn: no pool found for gameobject '{item.name}'");
            _poolsByID[_gameObjectNameToPoolID[item.name]].Despawn(item);
            _gameObjectNameToPoolID.Remove(item.name);
        }

        public void InitPool(string poolID) {
            if(!_poolsByID.ContainsKey(poolID))
                throw new Exception($"PrefabSpawner.PreloadPool: unknown '{poolID}'");
            _poolsByID[poolID].Init();
        }

        public void AddPool(string poolID, PrefabPool pool) {
            if(!_poolsByID.ContainsKey(poolID))
                throw new Exception($"PrefabSpawner.AddPool: pool ID collision '{poolID}'");
            _pools.Add(pool);
            _poolsByID.Add(poolID, pool);
        }

        public void Reset() {
            foreach (PrefabPool pool in _pools) 
                pool.Reset();
            _poolsByID.Clear();
            _gameObjectNameToPoolID.Clear();
        }

        #endregion

    
        public void Init(bool initAllPools = true) {
            foreach (var pool in _pools) {
                if (_poolsByID.ContainsKey(pool.ID))
                    throw new Exception($"PrefabSpawner.Init: pool ID collision '{pool.ID}'");
                _poolsByID.Add(pool.ID, pool);
                if(initAllPools)
                    pool.Init();
            }
        }

        private void OnDestroy() {
            Reset();
        }
    }
}