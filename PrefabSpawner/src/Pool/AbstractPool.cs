using System;
using System.Collections.Generic;
using UnityEngine;

namespace GotoAndPlay.Unity3D.Pool
{
    public abstract class AbstractPool<T>: ScriptableObject, IPool<T>
    {
        #region Properties

        [SerializeField] protected int _increasePoolChunkSize = 5;

        [SerializeField] protected int _initialPoolSize;
        
        [SerializeField] protected string _id;

        protected Stack<T> _store = new Stack<T>();

        protected Action<T> _onSpawn;
        
        protected Action<T> _onDespawn;
        
        protected Action<T> _onClear;

        #endregion

        #region IPool implementation

        /// <summary>
        /// Implementation must create pool's content
        /// </summary>
        protected abstract void createPool();
        public abstract T Spawn();
        public abstract void Despawn(T item);
        public abstract void Clear();
        
        public string ID
        {
            get => _id;
            set => _id = value;
        }

        public int InitialPoolSize
        {
            get => _initialPoolSize;
            set => _initialPoolSize = value;
        }

        public void Init() {
            if(poolIsEmpty())
                createPool();
        }

        public virtual void Reset() {
            Clear();
            _store.Clear();
        }

        public Action<T> OnSpawn
        {
            get => _onSpawn;
            set => _onSpawn = value;
        }

        public Action<T> OnDespawn
        {
            get => _onDespawn;
            set => _onDespawn = value;
        }

        public Action<T> OnClear
        {
            get => _onClear;
            set => _onClear = value;
        }

        #endregion

        protected bool poolIsEmpty() {
            return _store.Count == 0;
        }
        
        
        protected T getInstance() {
            if (poolIsEmpty())
                PopulatePool();
            return _store.Pop();
        }

        private void PopulatePool() {
            for (int i = 0; i < _increasePoolChunkSize; i++) {
                _store.Push(instanciateInPool());
            }
        }
        
        protected abstract T instanciateInPool();

    }
}