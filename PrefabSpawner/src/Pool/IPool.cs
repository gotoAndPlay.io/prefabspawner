using System;

namespace GotoAndPlay.Unity3D.Pool
{
    public interface IPool<T>
    {
        string ID { get; set; }
        T Spawn();
        void Despawn(T item);
        void Init();
        void Reset();
        void Clear();
        /// <summary>
        /// Action app
        /// </summary>
        Action<T> OnSpawn { get;set; }
        Action<T> OnDespawn { get;set; }
        Action<T> OnClear { get;set; }
    }
    
    
}