using UnityEngine;

namespace GotoAndPlay.Unity3D.Pool
{
    [CreateAssetMenu(fileName = "PrefabPool", menuName = "GotoAndPlay/Pool/PrefabPool", order = 1)]
    public class PrefabPool: AbstractPool<GameObject>, IPrefabPool
    {
        #region Properties

        [SerializeField] protected GameObject _prefab;

        [SerializeField]  protected Transform _poolContainer;
        
        [Space]
        
        [SerializeField] protected bool _disableObjectsInPool = false;
        
        [SerializeField] protected Vector3 _positionInPool = Vector3.zero;
        
        [SerializeField] protected Quaternion _rotationInPool = Quaternion.identity;
        
        protected int _itemCounter = 0;
        
        #endregion
        
        #region Getter & Setter
        
        public bool DisableObjectsInPool
        {
            get => _disableObjectsInPool;
            set => _disableObjectsInPool = value;
        }

        public Vector3 PositionInPool
        {
            get => _positionInPool;
            set => _positionInPool = value;
        }

        public Transform PoolContainer
        {
            get => _poolContainer;
            set => _poolContainer = value;
        }

        public GameObject Prefab
        {
            get => _prefab;
            set => _prefab = value;
        }

        #endregion

        #region IPrefabPool implementation

        protected override void createPool() {
            if (poolIsEmpty()) {
                for (int i = 0; i < _initialPoolSize; i++)
                    _store.Push(instanciateInPool());
            }
        }
        
        public override GameObject Spawn() {
            var item =  Spawn(_poolContainer, Vector3.zero, Quaternion.identity);
            if (_onSpawn != null)
                _onSpawn(item);
            return item;
        }

        public GameObject Spawn(Transform parent, Vector3 position, Quaternion rotation) {
            GameObject go = getInstance();
            
            var t = go.transform;
            t.parent = parent;
            t.SetPositionAndRotation(position, rotation);
            
            if (_disableObjectsInPool)
                go.SetActive(true);
            
            if (_onSpawn != null)
                _onSpawn(go);
            
            return go;
        }

        public override void Despawn(GameObject go) {
            var t = go.transform;
            t.parent = _poolContainer;
            t.SetPositionAndRotation(_positionInPool, Quaternion.identity);
            
            if (_onDespawn != null)
                _onDespawn(go);
            
            if (_disableObjectsInPool)
                go.SetActive(false);
            _store.Push(go);
        }

        public override void Clear() {
            foreach (var gameObject in _store) {
                if (_onClear != null)
                    _onClear(gameObject);
                    Destroy(gameObject);
            }

            _itemCounter = 0;
        }

        #endregion

        #region Protected

        protected override GameObject instanciateInPool() {
            _itemCounter++;
            var go = Instantiate(_prefab, _positionInPool, Quaternion.identity, _poolContainer);
            go.SetActive(!_disableObjectsInPool);
            go.name = buildInstanceName(go);
            return go;
        }
        
        protected string buildInstanceName(GameObject item) {
            return $"Item {_id} #{_itemCounter}";
        }

        #endregion
    }
}