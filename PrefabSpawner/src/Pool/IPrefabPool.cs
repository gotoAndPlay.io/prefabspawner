using UnityEngine;

namespace GotoAndPlay.Unity3D.Pool
{
    public interface IPrefabPool: IPool<GameObject>
    {
        GameObject Spawn(Transform parent, Vector3 position, Quaternion rotation);
        Transform PoolContainer {get;}
    }
}